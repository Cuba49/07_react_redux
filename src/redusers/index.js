import { combineReducers } from "redux";
import messages from "../components/MessageList/reducer";
import editMessageForm from "../components/EditMessageForm/reduser";
const rootReducer = combineReducers({
	messages,
	editMessageForm,
});

export default rootReducer;
