import React, { Component } from "react";
import "./style.css";
import { connect } from "react-redux";
import { addMessage } from "../MessageList/actions";
import { setCurrentMessageId, showForm } from "../EditMessageForm/actions";
class MessageInput extends Component {
	constructor(props) {
		super(props);
		this.state = {
			text: "",
		};
		this.onAdd = this.onAdd.bind(this);
		this.onKeyPress = this.onKeyPress.bind(this);
		document.addEventListener("keydown", this.onKeyPress);
	}
	onKeyPress(e) {
		if (e.key === "ArrowUp") {
			const lastMessage = this.props.messages.find(
				(message) => message.userId === this.props.myId
			);
			this.props.setCurrentMessageId(lastMessage.id);
			this.props.showForm();
		}
	}
	onChange(e) {
		const value = e.target.value;
		this.setState({ text: value });
	}
	onAdd() {
		if (this.state.text !== "") {
			this.props.addMessage({
				text: this.state.text,
				createdAt: new Date().toISOString(),
				userId: this.props.myId,
			});
			this.setState({ text: "" });
		}
	}
	render() {
		return (
			<div className="message-input">
				<textarea
					className="message-input-text"
					type="text"
					placeholder="Type your message"
					onChange={(e) => this.onChange(e)}
					value={this.state.text}
				/>
				<button className="message-input-button" onClick={this.onAdd}>
					SEND
				</button>
			</div>
		);
	}
}
const mapStateToProps = (state) => {
	return {
		messages: state.messages.messages,
	};
};

const mapDispatchToProps = {
	addMessage,
	setCurrentMessageId,
	showForm,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);
