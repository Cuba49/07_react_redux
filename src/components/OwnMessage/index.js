import React from "react";
import "./style.css";
import { convertTime } from "../../helpers/dateHelper";
class OwnMessage extends React.Component {
	render() {
		const message = this.props.message;
		return (
			<div className="own-message">
				<div className="message-body message-body-my">
					<p className="message-text">{message.text}</p>
					<div className="message-time">
						{convertTime(message.createdAt)}
					</div>
				</div>
				<i
					className="fas fa-edit message-edit"
					onClick={() => this.props.onEdit(message.id)}
				></i>
				<i
					className="far fa-trash-alt message-delete"
					onClick={() => this.props.onDelete(message.id)}
				></i>
			</div>
		);
	}
}
export default OwnMessage;
