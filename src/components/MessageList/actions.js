import service from "../../services/service";
import {
	ADD_MESSAGE,
	UPDATE_MESSAGE,
	DELETE_MESSAGE,
	SET_MESSAGES,
	LOAD_MESSAGES,
} from "./actionTypes";
import fetch from "cross-fetch";

export const addMessage = (data) => ({
	type: ADD_MESSAGE,
	payload: {
		id: service.getNewId(),
		data,
	},
});

export const updateMessage = (id, data) => ({
	type: UPDATE_MESSAGE,
	payload: {
		id,
		data,
	},
});

export const deleteMessage = (id) => ({
	type: DELETE_MESSAGE,
	payload: {
		id,
	},
});

export const setMessages = (array) => ({
	type: SET_MESSAGES,
	payload: { newMessages: array },
});
export const messagesIsLoad = () => ({
	type: LOAD_MESSAGES,
});
export const getMessages = (url) => {
	return (dispatch) => {
		dispatch({
			type: LOAD_MESSAGES,
		});
		return fetch(url)
			.then((response) => response.json())
			.then((json) => {
				dispatch({
					type: SET_MESSAGES,
					payload: {
						newMessages: json.sort(
							(a, b) => new Date(b.createdAt) - new Date(a.createdAt)
						),
					},
				});
			});
	};
};
