import React, { Component } from "react";
import { connect } from "react-redux";
import "./style.css";
import Message from "../Message";
import OwnMessage from "../OwnMessage";
import MessagesDivider from "../MessagesDivider";
import * as actions from "./actions";
import { setCurrentMessageId, showForm } from "../EditMessageForm/actions";
import { convertToEngDate } from "../../helpers/dateHelper";

class MessageList extends Component {
	constructor(props) {
		super(props);
		this.onEdit = this.onEdit.bind(this);
		this.onDelete = this.onDelete.bind(this);
		this.props.getMessages(this.props.url);
	}

	onEdit(id) {
		this.props.setCurrentMessageId(id);
		this.props.showForm();
	}

	onDelete(id) {
		this.props.deleteMessage(id);
	}

	render() {
		const myId = this.props.myId;
		const messagesBlocks = [];
		let contextDate = null;
		this.props.messages.forEach((message) => {
			if (
				convertToEngDate(message.createdAt) !== contextDate &&
				contextDate !== null
			) {
				messagesBlocks.unshift(
					<MessagesDivider date={contextDate} key={message.createdAt} />
				);
			}
			contextDate = convertToEngDate(message.createdAt);
			messagesBlocks.unshift(
				message.userId === myId ? (
					<OwnMessage
						key={message.id}
						message={message}
						onDelete={this.onDelete}
						onEdit={this.onEdit}
					/>
				) : (
					<Message
						key={message.id}
						message={message}
						onEdit={this.onEdit}
					/>
				)
			);
		});
		if (contextDate !== null) {
			messagesBlocks.unshift(
				<MessagesDivider date={contextDate} key={contextDate} />
			);
		}
		return (
			<div className="message-list" id="message-list">
				<div className="message-list-scroll">{messagesBlocks}</div>
			</div>
		);
	}
}
const mapStateToProps = (state) => {
	return {
		messages: state.messages.messages,
		preloader: state.messages.preloader,
	};
};
const mapDispatchToProps = {
	...actions,
	setCurrentMessageId,
	showForm,
};
export default connect(mapStateToProps, mapDispatchToProps)(MessageList);
