import {
	ADD_MESSAGE,
	UPDATE_MESSAGE,
	DELETE_MESSAGE,
	SET_MESSAGES,
	LOAD_MESSAGES,
} from "./actionTypes";

const initialState = { messages: [], isLoadMessages: false };

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case ADD_MESSAGE: {
			const { id, data } = action.payload;
			const newMessage = { id, ...data };
			return { ...state, messages: [newMessage, ...state.messages] };
		}

		case UPDATE_MESSAGE: {
			const { id, data } = action.payload;
			const updatedMessages = state.messages.map((message) => {
				if (message.id === id) {
					return {
						...message,
						...data,
					};
				} else {
					return message;
				}
			});
			return { ...state, messages: updatedMessages };
		}

		case DELETE_MESSAGE: {
			const { id } = action.payload;
			const filteredMessages = state.messages.filter(
				(message) => message.id !== id
			);
			return { ...state, messages: filteredMessages };
		}

		case SET_MESSAGES: {
			const { newMessages } = action.payload;
			return { ...state, messages: newMessages, preloader: false };
		}
		case LOAD_MESSAGES: {
			return { ...state, preloader: true };
		}

		default:
			return state;
	}
}
