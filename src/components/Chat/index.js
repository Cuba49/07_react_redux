import React, { Component } from "react";
import "./style.css";
import Header from "../Header";
import MessageInput from "../MessageInput";
import MessageList from "../MessageList";
import Preloader from "../Preloader";
import EditMessageForm from "../EditMessageForm";
import { connect } from "react-redux";

class Chat extends Component {
	constructor() {
		super();
		this.state = {
			myId: "5328dba1-1b8f-11e8-9629-c7eca82aa7bd",
		};
	}

	getCountUsers() {
		const uniqueUsers = [];
		this.props.messages.forEach((message) => {
			const userId = message.userId;
			if (uniqueUsers.indexOf(userId) === -1) {
				uniqueUsers.push(userId);
			}
		});
		return uniqueUsers.length;
	}

	getLastMessageAt() {
		const sortMessages = this.props.messages;
		return sortMessages.length > 0 ? sortMessages[0].createdAt : null;
	}

	render() {
		const isLoad = this.props.preloader ? <Preloader /> : [];
		return (
			<div className="chat">
				<Header
					name="Вселенский заговор"
					countUsers={this.getCountUsers()}
					countMessages={this.props.messages.length}
					lastMessageAt={this.getLastMessageAt()}
				/>
				<MessageList url={this.props.url} myId={this.state.myId} />
				<MessageInput myId={this.state.myId} />
				<EditMessageForm />
				{isLoad}
			</div>
		);
	}
}
const mapStateToProps = (state) => {
	return {
		messages: state.messages.messages,
		preloader: state.messages.preloader,
	};
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
