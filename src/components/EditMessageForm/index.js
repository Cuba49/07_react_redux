import React, { Component } from "react";
import "./style.css";
import * as actions from "./actions";
import { updateMessage } from "../MessageList/actions";
import { connect } from "react-redux";

class EditMessageForm extends Component {
	constructor(props) {
		super(props);
		this.onCancel = this.onCancel.bind(this);
		this.onSave = this.onSave.bind(this);
		this.onChangeData = this.onChangeData.bind(this);
		this.state = {
			text: "",
		};
	}

	componentWillReceiveProps(nextProps) {
		if (
			nextProps.editMessageId !== this.props.editMessageId &&
			nextProps.editMessageId !== ""
		) {
			const message = this.props.messages.find(
				(message) => message.id === nextProps.editMessageId
			);
			this.setState({ text: message.text });
		}
	}
	onChangeData(e) {
		const value = e.target.value;
		this.setState({
			...this.state,
			text: value,
		});
	}
	onSave() {
		if (this.props.editMessageId) {
			this.props.updateMessage(this.props.editMessageId, this.state);
		}
		this.props.dropCurrentMessageId();
		this.props.hideForm();
		this.setState({ text: "" });
	}
	onCancel() {
		this.props.dropCurrentMessageId();
		this.props.hideForm();
	}

	render() {
		const isDisplay = `edit-message-modal ${
			this.props.editModal ? "modal-shown" : ""
		}`;
		return (
			<div className={isDisplay}>
				<div className="edit-message-body">
					<textarea
						className="edit-message-input"
						onChange={this.onChangeData}
						value={this.state.text}
					></textarea>
					<div className="edit-message-bottom">
						<button
							className="edit-message-close"
							onClick={this.onCancel}
						>
							CLOSE
						</button>
						<button className="edit-message-button" onClick={this.onSave}>
							SAVE
						</button>
					</div>
				</div>
			</div>
		);
	}
}
const mapStateToProps = (state) => {
	return {
		messages: state.messages.messages,
		editModal: state.editMessageForm.editModal,
		editMessageId: state.editMessageForm.editMessageId,
	};
};

const mapDispatchToProps = {
	...actions,
	updateMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMessageForm);
