import {
  SET_CURRENT_MESSAGE_ID,
  DROP_CURRENT_MESSAGE_ID,
  SHOW_FORM,
  HIDE_FORM,
} from "./actionsTypes";

export const setCurrentMessageId = (id) => ({
  type: SET_CURRENT_MESSAGE_ID,
  payload: {
    id,
  },
});

export const dropCurrentMessageId = () => ({
  type: DROP_CURRENT_MESSAGE_ID,
});

export const showForm = () => ({
  type: SHOW_FORM,
});

export const hideForm = () => ({
  type: HIDE_FORM,
});
