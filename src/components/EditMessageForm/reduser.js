import {
	SET_CURRENT_MESSAGE_ID,
	DROP_CURRENT_MESSAGE_ID,
	SHOW_FORM,
	HIDE_FORM,
} from "./actionsTypes";

const initialState = {
	editMessageId: "",
	editModal: false,
};
// eslint-disable-next-line
export default function (state = initialState, action) {
	switch (action.type) {
		case SET_CURRENT_MESSAGE_ID: {
			const { id } = action.payload;
			return {
				...state,
				editMessageId: id,
			};
		}
		case DROP_CURRENT_MESSAGE_ID: {
			return {
				...state,
				editMessageId: "",
			};
		}

		case SHOW_FORM: {
			return {
				...state,
				editModal: true,
			};
		}

		case HIDE_FORM: {
			return {
				...state,
				editModal: false,
			};
		}

		default:
			return state;
	}
}
