import React from "react";
import "./style.css";
import { convertDateTime } from "../../helpers/dateHelper";

class Header extends React.Component {
	render() {
		return (
			<div className="header">
				<div className="header-left">
					<p className="header-title">{this.props.name}</p>
					<p className="grey-text header-users-count">
						{this.props.countUsers}
					</p>
					<p className="grey-text">participants</p>
					<p className="grey-text header-messages-count">
						{this.props.countMessages}
					</p>
					<p className="grey-text">messages</p>
				</div>
				<div className="header-right grey-text">
					<p>last message </p>
					<p className="header-last-message-date">
						{convertDateTime(this.props.lastMessageAt)}
					</p>
				</div>
			</div>
		);
	}
}
export default Header;
